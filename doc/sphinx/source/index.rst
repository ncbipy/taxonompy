taxonompy documentation
=======================

Synopsis
--------

taxonompy is a small library to parse NCBI taxonomies. The library uses a taxon
model which attributes are set by the parser. Currently, NCBI is making
taxonomic information available in  XML only, therefore only am XML parser
exists so far.

Licence and Copyright
---------------------

``taxonompy`` is licensed under the `GNU Lesser General Public License v3 (LGPLv3)`_ or later.

.. toctree::
  :maxdepth: 2
  :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
