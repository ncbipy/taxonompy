# Taxonomist notes

## Observations

### Re-fetching despite database
In some cases taxonomist re-fetches taxids even if they just have been resolved. I suspect it's an issue with
aliases which are not read when loading an existing database.

Ashleigh's taxids:
573993,1869111,1970469,1304863,1304864,191217,12040,373548,103724,359812

## ToDo

 - let taxon return its rank
 - assemble clade lineages straight from taxid
 - Indices for taxid table: worthwhile?