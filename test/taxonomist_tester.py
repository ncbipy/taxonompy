#!/usr/bin/env python3
#-------------------------------------------------------------------------------
#  File: taxonomist_tester.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Copyright: 2018 The University of Sydney
#  Version: 0
#  Description:
#-------------------------------------------------------------------------------


import os
import sys

sys.path.insert(1, os.path.join(sys.path[0], '../../taxonompy/src'))
import taxonompy.parser.xmlparser

def main():

  s = taxonompy.parser.xmlparser.NcbiTaxoXmlParser()
  fh = open(sys.argv[1], 'r')
  r = s.parse(fh)
  fh.close()
  for i in r.queries:
    t = r.taxa[i]
    lvl = 0
    while t.parent_taxid is not None:
      print("{}{}".format('  '*lvl, r.taxa[t.taxid].dump()))
      t = r.taxa[t.parent_taxid]
      lvl += 1
    print("{}{}".format('  '*lvl, r.taxa[t.taxid].dump()))

if __name__ == '__main__':
  main()
